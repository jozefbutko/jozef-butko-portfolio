# Jozef Butko portfolio
A home for code of my [personal portfolio](http://www.jozefbutko.com)

## Setup
Fetch the code:
```bash
git clone git@bitbucket.org:jozefbutko/jozef-butko-portfolio.git
```
  
And then install all dependencies:
```bash
npm install && bower install
```

**Note:** If `npm install` fails during dependency installation it will be likely caused by `gulp-imagemin`. In that case remove `gulp-imagemin` dependency from `package.json`, run `npm install` again and then install `gulp-imagemin` separately with following command: `npm install gulp-imagemin --save-dev`


## 1. Watch files
```bash
gulp
```
- all SCSS/HTML will be watched for changes and injected into browser thanks to BrowserSync

## 2. Build production version
```bash
gulp build
```
- this will process following tasks:
* clean _build folder
* compile SASS files, minify and uncss compiled css
* copy and optimize images
* minify and copy all HTML files into $templateCache
* build index.html
* minify and copy all JS files
* copy fonts
* show build folder size

## 3. Start webserver without watch task
```bash
gulp server
```

## 4. Start webserver from build folder
```bash
gulp server-build
```

## Contact
Copyright (C) 2015 Jozef Butko  
[www.jozefbutko.com](http://www.jozefbutko.com/resume)  
[www.github.com/jbutko](http://www.github.com/jbutko)  
[@jozefbutko](http://www.twitter.com/jozefbutko)  

## Changelog
### 1.0.0
- init
25.07.2015