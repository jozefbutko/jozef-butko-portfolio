/**
 * Jozef Butko Portfolio
 *
 * @description Description
 * @author Jozef Butko // www.jozefbutko.com
 * @url www.jozefbutko.com
 * @version 1.0.0
 * @date May 2015
 * @license MIT
 */
;(function() {


  /**
   * Definition of the main app module and its dependencies
   */
  angular
    .module('jozefButko', [
      'ngRoute',
      'ui.router',
      'ngAnimate',
      'ngSanitize'
    ])
    .config(config);

  // safe dependency injection
  // this prevents minification issues
  config.$inject = ['$routeProvider', '$locationProvider', '$httpProvider', '$compileProvider', '$urlMatcherFactoryProvider'];

  /**
   * App routing
   *
   * You can leave it here in the config section or take it out
   * into separate file
   */
  function config($routeProvider, $locationProvider, $httpProvider, $compileProvider, $urlMatcherFactoryProvider) {

    $locationProvider.html5Mode(true);

    $httpProvider.interceptors.push('authInterceptor');

    $urlMatcherFactoryProvider.strictMode(false);

  }


  /**
   * xxx
   */
  angular.module('jozefButko')
    .factory('authInterceptor', authInterceptor);

  authInterceptor.$inject = ['$rootScope', '$q', '$injector', '$location'];

  function authInterceptor($rootScope, $q, $injector, $location) {

    // spinner implementation source:
    // http://stackoverflow.com/questions/16303973/set-rootscope-variable-on-httpintercept#answer-18918026

    $rootScope.showSpinner = false;
    $rootScope.http = null;

    return {

      // intercept every request
      request: function(config) {
        $rootScope.showSpinner = true;
        config.headers = config.headers || {};
        return config;
      },

      requestError: function (rejection) {
        $rootScope.http = $rootScope.http || $injector.get('$http');

        if ($rootScope.http.pendingRequests.length < 1) {
            $rootScope.showSpinner = false;
        }

        return $q.reject(rejection);
      },

      response: function (response) {
        $rootScope.http = $rootScope.http || $injector.get('$http');

        if ($rootScope.http.pendingRequests.length < 1) {
            $rootScope.showSpinner = false;
        }

        return response || $q.when(response);
      },

      // Catch 404 errors
      responseError: function(response) {
        $rootScope.http = $rootScope.http || $injector.get('$http');
        if ($rootScope.http.pendingRequests.length < 1) {
            $rootScope.showSpinner = false;
        }

        if (response.status === 404) {
          $location.path('/');
          return $q.reject(response);
        } else {
          return $q.reject(response);
        }
      }
    };
  }


  /**
   * Run block
   */
  angular.module('jozefButko')
    .run(run);

  run.$inject = ['$rootScope', '$location'];

  function run($rootScope, $location) {

    // cache URL
    var path = $location.path(),
        urlNoTrailing = path && path.substr(-1) === '/' ? path.substr(0, path.length - 1): path;

    if (urlNoTrailing === '/html/direction-aware-hover-effect-css3-jquery') {
      $location.path('blog/html/direction-aware-hover-effect-css3-jquery');
    }

    if (urlNoTrailing === '/git/git-basics-init-add-commit') {
      $location.search('lang', null );
      $location.path('blog/git/git-basics-init-add-commit');
    }

    // scroll to top on route change
    $rootScope.$on('$stateChangeSuccess',function(event, toState, toParams){

      $('html, body').animate({ scrollTop: 0 }, 150);

    });

  }


})();