;(function() {


	/**
	 * Place to store API URL or any other constants
	 *
	 * Inject CONSTANTS service as a dependency and then use like this:
	 * CONSTANTS.API_URL
	 */
  angular
  	.module('jozefButko')
    .constant('CONSTANTS', {
      'API_URL': 'http://www.yourAPIurl.com/'
    });


})();
