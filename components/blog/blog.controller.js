/**
 * Home controller
 *
 * @desc Fetch portfolio data
 */
;(function() {


  angular.module('jozefButko')
    .controller('blogController', blogController);


  blogController.$inject = ['fetchService', '$location', 'blogPosts'];


  //////////////// controller


  function blogController(fetchService, $location, blogPosts) {


    var self = this;


    // cache URL
    var path = $location.path(),
        url = path ? path.split('/blog/')[1] : undefined,
        urlNoTrailing = url && url.substr(-1) === '/' ? url.substr(0, url.length - 1): url;

    // redirect old wordpress blog links to new angular links
    if (urlNoTrailing === '/html/direction-aware-hover-effect-css3-jquery') {
      $location.path('blog/html/direction-aware-hover-effect-css3-jquery');
    }

    if (urlNoTrailing === '/git/git-basics-init-add-commit') {
      $location.path('blog/git/git-basics-init-add-commit');
    }

    /**
     * load portfolio data, skills, featured etc.
     */
    // blogPosts are resolved in routing
    self.blogPosts = blogPosts.data;
    self.slugs = [];
    self.blogPosts.map(function(post) {
      // extract slug from url and remove final slash
      var slug = post.link.split('http://www.jozefbutko.com/')[1].slice(0, -1);
      self.slugs.push(slug);
    });


    /**
     * if we are on a detail page store single post in variable
     */
    if (url) {
      self.blogPosts.filter(function(post) {
        var postSlug = post.link.split('http://www.jozefbutko.com/')[1].slice(0, -1);
        if (postSlug === urlNoTrailing) {
          self.postDetail = post;
        }
      });
    }
  }


})();
