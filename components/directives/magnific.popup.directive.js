;(function() {

  'use strict';

  /**
   * Magnific Popup Directive by Jozef Butko
   *
   * @desc Zoom your gallery thumbnails to full screen images
   * @source http://dimsemenov.com/plugins/magnific-popup/
   * @author Jozef Butko
   * @ngdoc Directive
   * @date 08/09/2014
   *
   * @example
   * <main-nav><main-nav/>
   *
   */

  angular.module('jozefButko')
    .directive('magnificPopup', function() {
      return {
        // Restrict it to be an attribute in this case
        restrict: 'AEC',
        replace: false,
        transclude: false,
        priority: 1001,
        // responsible for registering DOM listeners as well as updating the DOM
        // fullscreen gallery
        link: function(scope, element, attrs) {

          var blogSection = attrs.magnificBlog;

          var opts = {
            delegate: 'a',
            type: 'image',
            preloader: true,
            closeBtnInside: false,
            fixedContentPos: true,
            closeOnContentClick: true,
            mainClass: 'mfp-with-zoom', // this class is for CSS animation below
            gallery: {
              enabled: !blogSection,
              navigateByImgClick: false
            },
            image: {
              verticalFit: true
            }
          };

          setTimeout(function() {
            $(element).magnificPopup(opts);
          }, 0);
        }
      };
    });

})();
