;(function() {

  'use strict';

  /**
   * Scroll To Top Directive by Jozef Butko
   *
   * @desc Get the user up
   * @author Jozef Butko
   * @ngdoc Directive
   * @date 22/06/2016
   *
   * @example
   * <scroll-top></scroll-top>
   *
   */

  angular.module('jozefButko')
    .directive('scrollTop', function() {
      return {
        restrict: 'E',
        replace: true,
        template: '<div class="ion-plane pull-right" title="Please take me up, sir!"></div>',
        link: function(scope, element, attrs) {
          element.on('click', function() {
            $('body').animate({scrollTop: 0}, 'ease');
          });
        }
      };
    });

})();
