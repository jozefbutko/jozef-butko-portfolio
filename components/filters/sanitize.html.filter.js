;(function() {


  /**
   * Sanitize HTML coming from API
   */


  'use strict';


  angular.module('jozefButko')
    .filter('unsafe', unsafeFilter);


  unsafeFilter.$inject = ['$sce'];


  //////////////// filter


  function unsafeFilter($sce) {
  	return $sce.trustAsHtml;
  }


})();
