/**
 * Home controller
 *
 * @desc Fetch portfolio data
 */
;(function() {


  angular.module('jozefButko')
    .controller('homeController', homeController);


  homeController.$inject = ['fetchService', 'queryService', '$rootScope'];


  //////////////// controller


  function homeController(fetchService, queryService, $rootScope) {

    // 'controller as'
    var self = this;

    $rootScope.home = true;

    /**
     * Load portfolio data, skills, featured etc.
     */
    fetchService.getPortfolio().then(function(data) {
      self.portfolioItems = data.data;
    });

    // since when am I into web development?
    self.careerYears = fetchService.activeWork();

    // show actual year in footer
    $rootScope.actualYear = self.careerYears.currentYear;
  }


})();