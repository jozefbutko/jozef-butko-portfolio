;(function() {


  /**
   * Service responsible for fetching portfolio data
   *
   * Features:
   * (1) Fetch portfolio items
   * (2) Dynamic calculation of career years
   *
   * @author  Jozef Butko
   *
   */


  'use strict';


  angular.module('jozefButko')
    .factory('fetchService', fetchService);


  fetchService.$inject = ['$http'];


  //////////////// factory


  function fetchService($http) {


    /**
     * Public API
     */
    var service = {
      getPortfolio: getPortfolio,
      activeWork: activeWork,
      getBlog: getBlog
    };
    return service;


    //////////////// function definitions


    /**
     * Fetch user profile
     *
     * @return {object} portfolio Portfolio items
     */
    function getPortfolio() {
      var req = {
        method: 'GET',
        url: '/assets/data/appData.json',
        cache: false
      };

      return $http(req).then(function(data) {
          return data;
        })
        .catch(function(data, status, headers, config) {
        });
    }

    /**
     * Fetch blog posts
     *
     * @return {object} posts Blog posts
     */
    function getBlog() {
      var req = {
        method: 'GET',
        url: 'http://www.jozefbutko.com/wp-json/posts',
        cache: false
      };

      return $http(req).then(function(posts) {
          return posts;
        })
        .catch(function(err) {});
    }



    /**
     * Dynamic calculation of career years
     *
     * @return {array} careerYears Career years
     */
    function activeWork() {
      var careerYears = {};

      careerYears.startDate = new Date(2012, 9, 1); //Create start date object by passing appropiate argument
      careerYears.endDate = new Date();
      careerYears.currentYear = careerYears.endDate.getFullYear();
      careerYears.totalMonths = (careerYears.currentYear - careerYears.startDate.getFullYear())*12 + (careerYears.endDate.getMonth() - careerYears.startDate.getMonth());
      careerYears.years = Math.floor(careerYears.totalMonths / 12);
      careerYears.yearsWord = careerYears.years === 1 ? 'year' : 'years';
      careerYears.months = careerYears.totalMonths % 12;

      return careerYears;
    }



  }


})();
