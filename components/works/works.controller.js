/**
 * Home controller
 *
 * @desc Fetch portfolio data
 */
;(function() {


  angular.module('jozefButko')
    .controller('worksController', worksController);


  worksController.$inject = ['fetchService', 'queryService', '$rootScope',
    '$stateParams', '$state', '$location'];


  //////////////// controller


  function worksController(fetchService, queryService, $rootScope,
    $stateParams, $state, $location) {

    // 'controller as'
    var self = this;


    $rootScope.home = false;

    /**
     * Load portfolio data, skills, featured etc.
     */
    fetchService.getPortfolio().then(function(data) {

      self.portfolioItems = data.data;

      // Base for filtered items
      var filteredData = [];

      // Loop through every projectItem and push it to filteredData array
      angular.forEach(self.portfolioItems.projects.projectItems, function(value , key) {

        // On the page refresh load only portfolio thumbnails with URL parameter tag (eg. /CSS)
        if (_.contains(value.tags, $state.params.tag)) {
          filteredData.push(value);

          // overwrite description according to first loaded thumbnail
          self.itemTitleToShow = filteredData[0].CSSid;

        // else if parameter tag === 'All' load all thumbnails
        } else if ( $state.params.tag === 'All' ) {
          filteredData.push(value);

          // overwrite description according to first loaded thumbnail
          self.itemTitleToShow = filteredData[0].CSSid;

        // else if we come from root (/) page there will be no state.params so load all thumbnails
        } else if ( $state.params.tag === undefined && $state.params.CSSid === undefined ) {
          filteredData.push(value);

          // set default project to ffo
          self.itemTitleToShow = 'ffo';

        } else if (_.contains(value.CSSid, ($state.params.tag === undefined) && $state.params.CSSid)) {
          filteredData.push(value);

          self.itemTitleToShow = filteredData[0].CSSid;
        }

        // assign filteredData to $scope
        self.filteredData = filteredData;

      });
    });


    // default portfolio item which will be shown after refresh/initial page load
    self.itemTitleToShow = 'ffo';
    self.showPortfolioItem = function(CSSid) {

      // Now get the CSSid value of clicked portfolio image and open corresponding description
      self.itemTitleToShow = CSSid;
      $location.path('works/' + CSSid);
    };

    self.portfolioFilter = function(tag) {

      // empty filterData array
      var filteredData = [];

      angular.forEach(self.portfolioItems.projects.projectItems, function(value, key) {

        // If the projectItem's tag array contains value of clicked tag add whole projectItem object to the 'filteredData' array
        if (_.contains(value.tags, tag)) {
          filteredData.push(value);
          // In case we click on 'All' tag load all thumbnails
        } else if (tag === 'All') {
          filteredData.push(value);
        }

      });

      // filtered data object containing only objects that contains clicked tag
      self.filteredData = filteredData;

      // set description according to first thumbnail
      self.itemTitleToShow = filteredData[0].CSSid;

    };



  }


})();