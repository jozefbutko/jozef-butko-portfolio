;(function() {


  'use strict';


  /**
   * Provides all routing with the applications with 404 logic,
   * login and authorizations
   *
   * @ngdoc config
   *
   */
  angular.module('jozefButko')
    .config(RoutingConfig);


  RoutingConfig.$inject = ['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider'];


  //////////////// definition


  /**
   * angular ui-router takes care about app routing
   * @link http://angular-ui.github.io/ui-router/site/#/api/ui.router
   * @tutorial https://scotch.io/tutorials/angular-routing-using-ui-router
   */
  function RoutingConfig($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

    $locationProvider.html5Mode(true);

    $httpProvider.interceptors.push('authInterceptor');

    // for any unmatched url, redirect to /state1
    $urlRouterProvider.otherwise('/');

    // now set up the states
    $stateProvider

      // home
      .state('home', {
        url: '/',
        templateUrl: 'components/home/home.html',
        controller: 'homeController',
        controllerAs: 'home'
      })

      // signup activation error
      .state('works', {
        url: '/works',
        templateUrl: 'components/works/works.html',
        controller: 'worksController',
        controllerAs: 'works'
      })

      // tag state - after clicking on filter tag
      .state('works.tag', {
          url: "/:tag/:CSSid",
          controller: 'worksController',
          controllerAs: 'works'
      })

      .state('works.single', {
          url: "/:CSSid",
          controller: 'worksController',
          controllerAs: 'works'
      })

      .state('blog', {
          url: "/blog",
          templateUrl: 'components/blog/blog.html',
          controller: 'blogController',
          controllerAs: 'blog',
          resolve: {
            blogPosts: ['fetchService', function(fetchService) {
              return fetchService.getBlog();
            }]
          }
      })

      .state('blogDetail', {
          url: "/blog/:blogTag/:blogId",
          templateUrl: 'components/blog/blog-detail.html',
          controller: 'blogController',
          controllerAs: 'blog',
          resolve: {
            blogPosts: ['fetchService', function(fetchService) {
              return fetchService.getBlog();
            }]
          }
      })

      // fix: redirect original WP links without /blog/ path to
      // AngularJS /blog/ path
      .state('originalBlogRoutingFixHtml', {
          // abstract: true,
          url: "/html/:link",
          templateUrl: 'components/blog/blog-detail.html',
          controller: 'blogController',
          controllerAs: 'blog',
          resolve: {
            blogPosts: ['fetchService', function(fetchService) {
              return fetchService.getBlog();
            }]
          }
      })

      .state('originalBlogRoutingFixGit', {
          // abstract: true,
          url: "/git/:link",
          templateUrl: 'components/blog/blog-detail.html',
          controller: 'blogController',
          controllerAs: 'blog',
          resolve: {
            blogPosts: ['fetchService', function(fetchService) {
              return fetchService.getBlog();
            }]
          }
      });
  }


})();