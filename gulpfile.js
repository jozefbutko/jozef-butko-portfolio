/**
 * @author  Jozef Butko
 * @url		  www.jozefbutko.com/resume
 * @date    March 2015
 * @license MIT
 *
 * AngularJS Boilerplate: Build, watch and other useful tasks
 *
 * The build process consists of following steps:
 * 1. clean /_build folder
 * 2. compile SASS files, minify and uncss compiled css
 * 3. copy and minimize images
 * 4. minify and copy all HTML files into $templateCache
 * 5. build index.html
 * 6. minify and copy all JS files
 * 7. copy fonts
 * 8. show build folder size
 *
 */
var gulp            = require('gulp'),
    browserSync     = require('browser-sync'),
    reload          = browserSync.reload,
    $               = require('gulp-load-plugins')(),
    del             = require('del'),
    runSequence     = require('run-sequence'),
    packageJson     = require('./package.json'),
    modRewrite      = require('connect-modrewrite'),
    bowerFiles      = require('main-bower-files'),
    pngquant        = require('imagemin-pngquant');


// optimize images
gulp.task('images', function() {
  return gulp.src('./assets/images/**/*.*')
    .pipe($.changed('./_build/assets/images'))
    // .pipe($.imagemin({
    //   progressive: true,
    //   svgoPlugins: [{
    //     removeViewBox: false
    //   }],
    //   use: [pngquant()]
    // }))
    //.pipe($.image())
    .pipe(gulp.dest('./_build/assets/images'));
});

// optimize wp images
gulp.task('wp-images', function () {
    return gulp.src('./wp-images/*')
        // .pipe(imagemin({
        //     progressive: true,
        //     svgoPlugins: [{removeViewBox: false}],
        //     use: [pngquant()]
        // }))
        // .pipe(imagemin({
        //   optimizationLevel: 3,
        //   progressive: true,
        //   interlaced: true
        // }))
        // .pipe(pngquant({quality: '65-80', speed: 4}))
        // .on('error', gutil.log)
        // .pipe(optipng({optimizationLevel: 3}))
        // .on('error', gutil.log)
        //.pipe(jpegoptim({max: 70}))
        .pipe($.image())
        .pipe(gulp.dest('./_build/wp-images'));
});

// browser-sync task, only cares about compiled CSS
gulp.task('browser-sync', function() {
  browserSync({
    server: {
      baseDir: "./"
    }
  });
});

// minify JS
gulp.task('minify-js', function() {
  gulp.src('js/*.js')
    .pipe($.uglify())
    .pipe(gulp.dest('./_build/'));
});

// minify build JS
gulp.task('minify-build-js', function() {
  gulp.src(['_build/js/angularlibs.js', '_build/js/appcomponents.js', '_build/js/templates.js'])
    //.pipe(uglify())
    .pipe($.concat('scripts.min.js'))
    .pipe($.rev())
    // .pipe(htmlreplace({
    //     scripts: {
    //         src: [['angularlibs.js', 'appcomponents', 'mainapp.js', 'templates.js']],
    //         tpl: '<script async src="%f".js></script>'
    //     }
    // }))
    .pipe(gulp.dest('./_build/js'));
});

gulp.task('minify-final-build', function() {
  gulp.src('/_build/js/scripts.min-a787141a.js')
    .pipe(uglify())
    .pipe(gulp.dest('./_build/js1'));
});


// minify CSS
gulp.task('minify-css', function() {
  gulp.src(['./styles/**/*.css', '!./styles/**/*.min.css'])
    .pipe($.rename({suffix: '.min'}))
    .pipe($.minifyCss({keepBreaks:true}))
    .pipe(gulp.dest('./styles/'))
    .pipe(gulp.dest('./_build/css/'));
});

/**
 * minify HTML
 */
gulp.task('minify-build-html', function() {
  var opts = {
    comments: true,
    spare: true,
    conditionals: true
  };

  gulp.src('./_build/index.html')
    .pipe($.minifyHtml(opts))
    .pipe(gulp.dest('./_build/'));
});

// copy fonts from a module outside of our project (like Bower)
gulp.task('fonts', function() {
  gulp.src('./assets/fonts/*')
    .pipe($.changed('./_build/assets/fonts'))
    .pipe(gulp.dest('./_build/assets/fonts'));
});

// copy fonts from a module outside of our project (like Bower)
// gulp.task('db', function() {
//   gulp.src('./assets/data/**/appData.json')
//     .pipe($.changed('./_build/assets/data'))
//     .pipe(gulp.dest('./_build/assets/data'));
// });

// copy json db
gulp.task('db', function () {
    return gulp.src(['./assets/data/appData.json'])
        .pipe($.jsonminify())
        .pipe(gulp.dest('./_build/assets/data/'));
});

// start webserver
gulp.task('server', function(done) {
  return browserSync({
    server: {
      baseDir: './',
      middleware: [
          modRewrite(['^([^.]+)$ /index.html [L]'])
      ]
    }
  }, done);
});

// start webserver from _build folder to check how it will look in production
gulp.task('server-build', function(done) {
  return browserSync({
    server: {
      baseDir: './_build/',
      middleware: [
          modRewrite(['^([^.]+)$ /index.html [L]'])
      ]
    }
  }, done);
});

// delete build folder
gulp.task('clean:build', function (cb) {
  del([
    './_build/'
    // if we don't want to clean any file we can use negate pattern
    //'!dist/mobile/deploy.json'
  ], cb);
});

// concat files
gulp.task('concat', function() {
  gulp.src('./js/*.js')
    .pipe($.concat('scripts.js'))
    .pipe(gulp.dest('./_build/'));
});

/**
 * inject app components
 */
gulp.task('inject', function() {
  return gulp.src('index.html')
    .pipe($.inject(
      gulp.src([
        'app.js',
        'app/**/*.js',
        '!app/**/*.spec.js',
        '!app/**/*.mock.js',
        'core/**/*.js',
        '!core/**/*.spec.js',
        '!core/**/*.mock.js',
        'components/**/*.js',
        '!components/**/*.spec.js',
        '!components/**/*.mock.js',
        'directives/**/*.js',
        '!directives/**/*.spec.js',
        '!directives/**/*.mock.js',
        'services/**/*.js',
        '!services/**/*.spec.js',
        '!services/**/*.mock.js'
      ])
      .pipe($.angularFilesort()),
      //.pipe($.print()) // print file stream
      {addRootSlash: false}
    ))
    .pipe(gulp.dest('./'))
    .pipe(gulp.dest('./_build'));
});


/**
 * inject bower dependencies
 */
gulp.task('inject-bower', function() {
  return gulp.src('index.html')
    .pipe($.inject(gulp.src(bowerFiles(), {
      read: false,
      addRootSlash: false
    }), {
      addRootSlash: false,
      name: 'bower'
    }))
    .pipe(gulp.dest('./'))
    .pipe(gulp.dest('./_build'));
});


// SASS task, will run when any SCSS files change & BrowserSync
// will auto-update browsers
gulp.task('sass', function() {
  return gulp.src('styles/style.scss')
    .pipe($.plumber()) // do not halt execution on error
    .pipe($.sourcemaps.init())
    .pipe($.sass({
      style: 'expanded',
      errLogToConsole: true,
      onError: function(err) {
        return $.notify().write(err);
      }
    }))
    .on('error', $.notify.onError({
      title: 'SASS Failed',
      message: 'Error(s) occurred during compile!'
    }))
    .pipe($.autoprefixer('last 3 version'))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('styles'))
    .pipe(reload({
      stream: true
    }))
    .pipe($.notify({
      message: 'Styles task complete'
    }));
});

// SASS Build task
gulp.task('sass:build', function() {
  var s = $.size();

  return gulp.src('styles/style.scss')
    .pipe($.sass({
      style: 'compact'
    }))
    .pipe($.autoprefixer('last 3 version'))
    // .pipe($.uncss({
    //   html: ['./index.html', './views/**/*.html', './components/**/*.html'],
    //   ignore: [
    //     '.index',
    //     '.slick',
    //     /\.owl+/,
    //     /\.owl-next/,
    //     /\.owl-prev/
    //   ]
    // }))
    .pipe($.minifyCss({
      keepBreaks: true,
      aggressiveMerging: false,
      advanced: false
    }))
    .pipe($.rename({suffix: '.min'}))
    .pipe(gulp.dest('_build/css'))
    .pipe(s)
    .pipe($.notify({
      onLast: true,
      message: function() {
        return 'Total CSS size ' + s.prettySize;
      }
    }));
});


// index.html build
// script/css concatenation
gulp.task('usemin', function() {
  return gulp.src('./index.html')
    // add templates path
    .pipe($.htmlReplace({
      templates: {
        src: 'js/templates',
        tpl: '<script type="text/javascript" src="%s.js"></script>'
      }
    }))
    .pipe($.usemin({
      css: [$.minifyCss(), 'concat'],
      angularlibs: [$.uglify()],
      appcomponents: [$.uglify()]
    }))
    .pipe(gulp.dest('./_build/'));
});


/**
 * make templateCache from all HTML files
 */
gulp.task('templates', function() {
  return gulp.src([
    './**/*.html',
    '!vendors/**/*.*',
    '!views/**/*.*',
    '!- source/**/*.*',
    '!report/**/*.*',
    '!node_modules/**/*.*',
    '!- backup/**/*.*',
    '!_build/**/*.*'
    ])
    .pipe($.minifyHtml())
    .pipe($.angularTemplatecache({
      module: 'jozefButko',
      filename: 'templates-' + packageJson.version + '.js'
    }))
    .pipe(gulp.dest('_build/js'));
});

/**
 * image sprites
 */
gulp.task('sprite', function() {
  var spriteData =
    gulp.src(['assets/images/**/*.png', 'assets/images/**/*.jpg', '!assets/images/sprite.png'])
    .pipe($.plumber()) // do not halt execution on error
    .pipe($.spritesmith({
      imgName: 'sprite.png',
      cssName: '_sprite.scss',
      imgPath: '../assets/images/sprite.png',
      cssFormat: 'css',
      algorithm: 'binary-tree'
    }));

    spriteData.img.pipe(gulp.dest('./assets/images/'));
    spriteData.css.pipe(gulp.dest('./styles/partials/'));
});

// reload all Browsers
gulp.task('bs-reload', function() {
  browserSync.reload();
});

/**
 * version bump
 */
gulp.task('bump:version', function() {
  gulp.src(['./bower.json', './package.json'])
    .pipe($.bump())
    .pipe(gulp.dest('./'));
});

// calculate build folder size
gulp.task('build:size', function() {
  var s = $.size();

  return gulp.src('./_build/**/*.*')
    .pipe(s)
    .pipe($.notify({
      onLast: true,
      message: function() {
        return 'Total build size ' + s.prettySize;
      }
    }));
});


// default task to be run with `gulp` command
// this default task will run BrowserSync & then use Gulp to watch files.
// when a file is changed, an event is emitted to BrowserSync with the filepath.
gulp.task('default', ['server', 'sass', 'minify-css', 'inject', 'inject-bower'], function() {
  gulp.watch('styles/*.css', function(file) {
    if (file.type === 'changed') {
      reload(file.path);
    }
  });
  gulp.watch(['app/*.js', 'app.js', 'components/**/*.js', 'directives/**/*.js', 'services/**/*.js', 'core/**/*.js', 'filters/**/*.js'], ['inject', 'bs-reload']);
  gulp.watch(['*.html', 'views/*.html', 'components/**/*.html',  'directives/**/*.html', '_build/index.html'], ['bs-reload']);
  gulp.watch(['styles/**/*.scss', 'directives/**/*.scss', 'components/**/*.scss'], ['sass', 'minify-css']);
  gulp.watch(['assets/images/**/*.png', 'assets/images/**/*.jpg', '!assets/images/sprite.png']);
  gulp.watch(['bower.json'], ['inject-bower', 'bs-reload']);
});

/**
 * build task:
 * 1. clean /_build folder
 * 2. inject app component scripts from modules/componets etc. folder
 * 3. inject bower dependecies
 * 4. compile SASS files and minify scss files
 * 5. make sprite image
 * 6. copy and minimize images
 * 7. copy and minimize favicons
 * 8. minify and copy all HTML files into $templateCache
 * 9. build index.html with usemin (minify and copy all JS files)
 * 10. minify _build/index.html
 * 11. copy fonts
 * 12. show build folder size
 * 13. bump build version
 * 14. zip build folder
 */
gulp.task('build', function(callback) {
  runSequence(
    'clean:build',
    'inject',
    'inject-bower',
    'sass:build',
    'images',
    'templates',
    'usemin',
    'fonts',
    'db',
    'build:size',
    'bump:version',
    callback);
});